0. Install CreoViewAdapters on Worker computer 
1. Copy GenericDocumentWorker to CreoViewAdapter installation, for example 'c:\ptc' folder on remote machine
2. Edit agent.ini file on Windchill server (<Windchill>\conf\wvs\conf\agent.ini).
 Note: Configure the worker as shapetype OFFICE (Note: Case sensitive)
2.1 Example:
------------------------------------------------------
[worker2]
distributed=false
autostart=true
host=<worker server network name>
autobusystop=
autoidlestop=
starttime=60
startfromlocal=FALSE
autoerrorstop=false
remotepath=C:\WC_shared_folder
hosttype=nt
localpath=\\<worker server network name>\wc_shared_folder$
exe=C:\PTC\GenericDocumentWorker\bin\workerstart.bat
password=
maxinstances=1
shapetype=OFFICE
prompt=
availabletime=
username=
port=601
;
-----------------------------------------------------


3. Configure WVS to recognise content on a Windchill Document (WTDoc), either by its content file extension <ext>
3.1 Edit site.xconf, add:

   <Property name="worker..DOC" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
   <Property name="worker..DOCX" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
   <Property name="worker..XLS" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
   <Property name="worker..XLSX" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
   <Property name="worker..PPT" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
   <Property name="worker..PPTX" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="OFFICE"/>
3.2 Run xconfmanager -pf
3.3 Or Add extension <ext> in Shell, for example 'xconfmanager -s "worker..DOCX=OFFICE" -t <Windchill>\codebase\wvs.properties -p'      (note the double '..')
4 Add path to whitelist to access on remote machine in site.xcoonf (Note: Case sensitive)
   <Property name="worker.exe.whitelist.prefixes" overridable="true"
             targetFile="codebase/WEB-INF/conf/wvs.properties"
             value="C:\PTC|c:ptc|c:\ptc|c:\PTC"/>
5 Set permission(read+write) on shared folder on the worker machine, for example \\<worker server network name>\wc_shared_folder$
6 Install java on worker machine and write path in workerstart.bat
7 Write server name in workerstart.bat
6 Create the wvs.jar file and copy to worker machine.
- Create the wvs.jar file from under the <Windchill>\codebase folder on server. Run the following command from a Windchill Shell: ant -f wvsMakeJar.xml
- Copy the resulting wvs.jar to the <worker server> <disk>:\ptc\GenericDocumentWorker\codebase 
- Modify the Generic Worker startup script, workerstart.bat, to match your installation
6 Restart Windchill
7 Check worker
8 If you have a problem (COM, DCOM permission, etc), please:
-read file <disk>:\ptc\GenericDocumentWorker\bin\readme.txt
-you can enable Debug in file <disk>:\ptc\GenericDocumentWorker\bin\workeroffice.vbs
