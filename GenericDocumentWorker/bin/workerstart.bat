@echo off
set GENERIC_INSTALL=C:\PTC\GenericDocumentWorker
set JAVA_HOME=C:\Program Files\Java\jre1.8.0_144
set PATH=%JAVA_HOME%;%JAVA_HOME%\bin
set CLASSPATH=%GENERIC_INSTALL%\codebase\wvs.jar;%GENERIC_INSTALL%\codebase
set DEBUG="-D"
set PORT="5600"
set HOST="<windchill_server>"
set TYPE="OFFICE"
set CMD="%GENERIC_INSTALL%\bin\run_vbs.bat"
set DIR="%GENERIC_INSTALL%\tmp"
set LOG="worker_"
java com.ptc.wvs.server.cadagent.GenericWorker %DEBUG% -PORT %PORT% -HOST %HOST% -TYPE %TYPE% -CMD %CMD% -DIR %DIR% -LOG %LOG% 
