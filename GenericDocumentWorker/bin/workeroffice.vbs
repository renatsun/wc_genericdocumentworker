Option Explicit

' Enable or Disable debug
Dim Debug
Debug=False


If WScript.Arguments.Count <2 Then
  WScript.Quit(1)
else
  WScript.Quit(Main)  
End If


Function Main()

  If  Wscript.Arguments(0)="" or Wscript.Arguments(1)="" Then
     Main=1
     Exit Function
  End If

'  If (WScript.Arguments.Count =1)  Then
'    sFilePath = Wscript.Arguments(0)
'  Else
'    sFilePath = Wscript.Arguments(0)
'    sPviewHome = Wscript.Arguments(1)
'  End If

  Dim oFSO, oFile, oOutFile, oInFile, oLogFile, sFilePath, sFolder, sBaseName, sExtFile, RetCode, sPviewHome, sInFile, sOutFile,  sArgLine, sArrArgs, sInDir, sLogFile
 
  sPviewHome="C:\PTC\creo_view_adapters"

  RetCode=0

  Set oFSO = CreateObject("Scripting.FileSystemObject")

  If Debug Then
    sLogFile = "C:\PTC\GenericDocumentWorker\tmp\GenericWorker.log"
    Set oLogFile = oFSO.CreateTextFile(sLogFile,True)
  End If
  
  sInFile = Wscript.Arguments(0)
  sOutFile = Wscript.Arguments(1)

  If Debug Then
    oLogFile.WriteLine "In File " & sInFile
    oLogFile.WriteLine "Out File " & sOutFile
  End If

  If (Not oFSO.FileExists(sOutFile)) Then
     Set oOutFile = oFSO.CreateTextFile(sOutFile,True)
  Else
     Set oOutFile = oFSO.OpenTextFile(sOutFile,2)
  End If

  oOutFile.Write("1 Failed during conversione")
  oOutFile.Close

  If (Not oFSO.FileExists(sInFile)) Then
     Set oOutFile = oFSO.OpenTextFile(sOutFile,2)
     oOutFile.Write("1 Failed to locate input file " & sInFile)
     oOutFile.Close
     Main=1
     Exit Function
  End If
  
  Set oInFile = oFSO.OpenTextFile(sInFile,1)
  Do Until oInFile.AtEndOfStream
    sArgLine = oInFile.Readline
  Loop
  oInFile.Close

  If Debug Then oLogFile.WriteLine "Arg: " & sArgLine

  sArrArgs=Split(sArgLine, " ")
  sInDir=sArrArgs(Ubound(sArrArgs) -1) 
  sFolder=sArrArgs(Ubound(sArrArgs)) 'Output Directory

  If Debug Then
    oLogFile.WriteLine "InDir: " & sInDir
    oLogFile.WriteLine "Folder: " & sFolder
  End If

  If (not oFSO.FolderExists(sFolder)) Then
     If Debug Then oLogFile.WriteLine "Create Folder: " & sFolder
     oFSO.CreateFolder sFolder
  End If

'DOC00001167_TTS_PD@_Project@_Results docx master_rep 6 C:\WC_shared_folder\w3i1j1 C:\WC_shared_folder\w3i1j1
'DOC00001167_TTS_PD Project Results.docx

  'output Basename and extension
  sBaseName=Replace(sArrArgs(0),"@", "")
  sBaseName=Replace(sBaseName,".", "_")
  sExtFile=sArrArgs(1)

  If Debug Then
    oLogFile.WriteLine "BaseName: " & sBaseName
    oLogFile.WriteLine "ExtFile: " & sExtFile
  End If

  'Source Filename
  sFilePath =sInDir& "\"&Replace(sArrArgs(0),"@_", " ")&"."&sExtFile
  'sFilePath =""""&sInDir& "\"&Replace(sArrArgs(0),"@_", " ")&"."&sExtFile&""""

  If Debug Then oLogFile.WriteLine "FilePath: " & sFilePath

  'Set oFile = oFSO.GetFile(sFilePath)
  'sFolder=oFSO.GetParentFolderName(oFile)
  'Wscript.Echo "Absolute path:"  & objFSO.GetAbsolutePathName(objFile)
  'sFileName=oFSO.GetFileName(oFile)
  'sBaseName=oFSO.GetBaseName(oFile)
  'sExtFile=oFSO.GetExtensionName(oFile)

  If (OfficeToPDF(sBaseName, sFolder, sExtFile, sFilePath)=0  and oFSO.FileExists(sFolder&"\"& sBaseName &"_"&sExtFile&".pdf")) Then
	RetCode=CreatePVS(sPviewHome, sFolder, sBaseName, sExtFile) 
	Set oOutFile = oFSO.OpenTextFile(sOutFile,2)
	If (RetCode =0) Then	 
	  oOutFile.Write("0 "&sFolder&"\"&sBaseName&"_"&sExtFile&".pvs")
          If Debug Then oLogFile.WriteLine "OK File: "&sFolder&"\"&sBaseName&"_"&sExtFile&".pvs"
	Else
	  oOutFile.Write("1 Failed to process data (Function CreatePVS)")
          If Debug Then oLogFile.WriteLine "Error in Function CreatePVS"
	End If
 	oOutFile.Close
  Else 
       Set oOutFile = oFSO.OpenTextFile(sOutFile,2)
       oOutFile.Write("1 Failed to process data (Function OfficeToPDF)" )
       If Debug Then oLogFile.WriteLine "Error in Function OfficeToPDF"
       oOutFile.Close
       RetCode =1
  End If

  If Debug Then oLogFile.Close

  Main=RetCode

End Function

Function OfficeToPDF(sBaseName, sFolder, sExtFile, sFilePath)

  'Office - export to PDF
  Dim oOffice, oWkbk, oPresentation, oDoc, RetCode, xmlDoc, tempObj, colNodes, objNode, propertyExists

  RetCode=0

On Error Resume Next

  Set xmlDoc = CreateObject("Microsoft.XMLDOM")
  xmlDoc.Async = "False"
  xmlDoc.Load(sFolder&"\wcdti.xml")
  Set colNodes=xmlDoc.selectNodes("//MSOIMIME/WCFile/Attribute")

  ' Save to PDF XLS document
  If (sExtFile = "xlsx" or sExtFile = "xls") Then  
    Set oOffice = CreateObject("Excel.Application")
    oOffice.Visible = False
    Set oWkbk = oOffice.Workbooks.Open(sFilePath,,True)
    'File not found
    If (oWkbk Is Nothing) Then
	oOffice.Quit 	
	OfficeToPDF=1
	Exit Function
    End If
    oWkbk.ExportAsFixedFormat 0, sFolder&"\"&sBaseName &"_"&sExtFile& ".pdf"
    oWkbk.Close 2
  ' Save to PDF WORD document
  elseIf (sExtFile = "docx" or sExtFile = "doc") Then
    Set oOffice = CreateObject("Word.Application")
    oOffice.Visible = false
    Set oDoc=oOffice.Documents.Open(sFilePath,,True)
    'File not found
    If (oDoc Is Nothing) Then
	oOffice.Quit 	
	OfficeToPDF=1
	Exit Function
    End If
    For Each objNode in colNodes
        propertyExists=False
	On Error Resume Next	
	Set tempObj = oDoc.CustomDocumentProperties.Item(objNode.Attributes.getNamedItem("key").Text)
	propertyExists = (Err.Number = 0)
	On Error Goto 0

	If (propertyExists=false) Then
           Err.Clear
	   oDoc.CustomDocumentProperties.Add objNode.Attributes.getNamedItem("key").Text, false, 4, objNode.Text
	Else
	   oDoc.CustomDocumentProperties(objNode.Attributes.getNamedItem("key").Text).Value = objNode.Text
	End If

    Next
    oDoc.Fields.Update
    oDoc.ExportAsFixedFormat sFolder&"\"&sBaseName &"_"&sExtFile& ".pdf",17,0,0
    oDoc.Close False
  ' Save to PDF PowerPoint document
  elseIf (sExtFile = "pptx" or sExtFile = "ppt") Then
    Set oOffice = CreateObject("PowerPoint.Application")
    Set oPresentation=oOffice.Presentations.Open(sFilePath, True,,False)
    'File not found
    If (oPresentation Is Nothing) Then
	oOffice.Quit 	
	OfficeToPDF=1
	Exit Function
    End If
    oPresentation.SaveAs sFolder&"\"&sBaseName&"_"&sExtFile,32
    oPresentation.Close  
  else
    'Unsupported filetype
     RetCode=1
  End If

  oOffice.Quit  
  Set oOffice=Nothing
  Set xmlDoc=Nothing

  If Err.Number <> 0 Then RetCode=1

  OfficeToPDF=RetCode

End Function


Function CreatePVS(sPviewHome, sFolder, sBaseName, sExtFile)
'Preview for Windchill
                  
  If (sPviewHome<>"") then
      Dim oShell,RetCode

      Set oShell = CreateObject("WScript.Shell")
      'Create PVS
      RetCode= oShell.Run(sPviewHome&"\bin\pvsmake -o "&sBaseName&"_"&sExtFile&" -p "&sFolder&" "&sFolder&"\"&sBaseName&"_"&sExtFile& ".pdf",0,True)
      ' if filename have '&' then pvsmake replace by '_'.
      ' rename back
      if InStr(sBaseName,"&") then
	Dim Fso
	Set Fso = WScript.CreateObject("Scripting.FileSystemObject")
	Fso.MoveFile sFolder&"\"&Replace(sBaseName,"&","_")&"_"&sExtFile&".pvs", sFolder&"\"&sBaseName&"_"&sExtFile&".pvs"
      End If
      If RetCode=0 then
      	'Create PVSThumb (jpg)
        RetCode= oShell.Run(sPviewHome&"\bin\pvsthumb -o "&sBaseName&"_"&sExtFile&" -p "&sFolder&" "&sFolder&"\"&sBaseName&"_"&sExtFile&".pvs",0,True)
      End If

      Set oShell = Nothing
  End If

  CreatePVS =RetCode
 
End Function   