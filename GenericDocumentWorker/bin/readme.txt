Steps to resolve most server side Excel(Word) automation issues.
1.��� Run the windows service (GS Worker Daemon) as a user who has permissions on excel. Windows services generally run using the local account and these are mostly not configured to open excel. Double click on your windows service in services.msc -> Choose Log on Tab -> Enter interactive user account and password.
2.�� �Excel looks for certain folders in the system.� A Desktop folder seems to be necessary in the systemprofile folder to open file by Excel. A lot of times, this solves a lot of excel automation issues.
For 64-bit (x64), create this folder: C:\Windows\SysWOW64\config\systemprofile\Desktop.
For 32-bit (x86), create this folder: C:\Windows\System32\config\systemprofile\Desktop
3.�� �Also DCOMConfig settings need to be changed. Follow the following steps to change DCOMConfig settings for excel.
Start -> dcomcnfg.exe (you might be prompted for administrator credentials) -> Console Root -> Component Services -> My Computer -> DCOM Config
Click �Microsoft Excel Application�. Right Click -> Properties.
Choose Identity tab. Select Interactive User.
Choose Security Tab. Customize �Launch and Activation Permissions�. Edit and add permissions to the user whom you are running the windows service.
Customize �Access Permissions�. Edit and add permissions to the user whom you are running the windows service.
Refer to the following site for details for more details on how to change the dcom config settings: http://forums.asp.net/t/1303594.aspx
4.�� �Follow the following steps if Microsoft Excel Application is not visible under dcomcnfg settings. On 64 bit system with 32 bit Office �
Start
Run
mmc -32
File
Add Remove Snap-in
Component Services
Add
OK
Console Root
Component Services
Computers
My Computer
DCOM Config
Microsoft Excel Application. Now perform the steps mentioned in previous step.
Apart from these steps, I have in extreme cases ended up modifying a few registry entries. But the above steps resolve most of the issues. Hope this article helps you to fix server side office automation deployment issues allowing you focus on real work!